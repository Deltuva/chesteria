## Front-End Home page

![Home page](https://www.dropbox.com/s/hd95a7bf3dze7ls/laptop_home.png?dl=1)

## Front-End Login#1 page

![Login 1](https://www.dropbox.com/s/piglpma9zjj9mwz/laptop_login1.png?dl=1)

## Front-End Login#2 page

![Login 2](https://www.dropbox.com/s/th4xbeope4j84t0/laptop_login2.png?dl=1)

## Front-End Responsive

![Mobile](https://www.dropbox.com/s/unynsy2ogzxthch/mobile.png?dl=1)

