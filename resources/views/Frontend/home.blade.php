<!doctype html>
<html lang="{{ app()->getLocale() }}">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Chesteria</title>

    <link rel="stylesheet" href="{{ mix('/css/app.css') }}">

</head>

<body>
    <header class="main__header">
        <!-- Modal Login -->
        <div class="modal fade" id="loginModal" role="dialog">
            <div class="modal-dialog">
                <!-- Modal content-->
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" data-dismiss="modal" aria-label="Close" class="close">
                            <span aria-hidden="true">×</span>
                        </button>
                        <h4 id="myModalLabel" class="modal-title">Login And Register</h4>
                    </div>
                    <div class="modal-body">
                        <div class="panel panel-login">
                            <div class="panel-logo"></div>
                            <div class="panel-heading">
                                <div class="row">
                                    <div class="col-xs-6 text-center">
                                        <a href="#" id="login-form-link" class="active">Login</a>
                                    </div>
                                    <div class="col-xs-6 text-center">
                                        <a href="#" id="register-form-link" class="">Register</a>
                                    </div>
                                </div>
                            </div>
                            <div class="panel-body">
                                <div class="row">
                                    <div class="col-lg-6" style="margin: 0 auto; float: none;">
                                        <form id="login-form" action="#" method="post" role="form">
                                            <div class="alert  alert-success" style="display: none;">
                                                <ul></ul>
                                            </div>
                                            <div class="form-logo"></div>
                                            <div class="form-group">
                                                <input name="name" id="name" tabindex="1" placeholder="Username" value="" class="form-control" type="text">
                                            </div>
                                            <div class="form-group">
                                                <input name="password" id="password" tabindex="2" placeholder="Password" class="form-control" type="password">
                                            </div>
                                            <div class="form-group text-center">
                                                <div class="checkbox checkbox-primary">
                                                    <input id="remember" name="remember" type="checkbox">
                                                    <label for="remember">
                                                        Remember me
                                                    </label>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <div class="row">
                                                    <div class="col-sm-6 col-sm-offset-3">
                                                        <input name="login-submit" id="login-submit" tabindex="4" value="Log In" class="form-control btn btn-login" type="submit">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <div class="row">
                                                    <div class="col-lg-12">
                                                        <div class="text-center">
                                                            <a href="#" tabindex="5" class="forgot-password">Forgot Password?</a>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </form>
                                        <form id="register-form" action="#" method="post" role="form" style="display: none;">
                                            <div class="alert  alert-success" style="display: none;">
                                                <ul></ul>
                                            </div>
                                            <div class="form-group">
                                                <input name="name" id="name" tabindex="1" placeholder="Username" value="" class="form-control" type="text">
                                            </div>
                                            <div class="form-group">
                                                <input name="email" id="email" tabindex="1" placeholder="Email Address" value="" class="form-control" type="email">
                                            </div>
                                            <div class="form-group">
                                                <input name="password" id="password" tabindex="2" placeholder="Password" class="form-control" type="password">
                                            </div>
                                            <div class="form-group">
                                                <input name="password_confirmation" id="password_confirmation" tabindex="2" placeholder="Confirm Password" class="form-control"
                                                    type="password">
                                            </div>
                                            <div class="form-group">
                                                <img src="/captcha">
                                                <button type="button" class="btn btn-xs">Reload</button>
                                                <input name="captcha" id="captcha" tabindex="2" placeholder="Code from image" class="form-control" type="text">
                                            </div>
                                            <div class="form-group">
                                                <div class="row">
                                                    <div class="col-sm-6 col-sm-offset-3">
                                                        <input name="register-submit" id="register-submit" tabindex="4" value="Register Now" class="form-control btn btn-register"
                                                            type="submit">
                                                    </div>
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>
        <section class="header__container mobile-container">
            <div class="container-fluid">
                <div class="header__container-content">
                    <div class="hamburger-menu hidden-lg">
                        <div class="one"></div>
                        <div class="two"></div>
                        <div class="three"></div>
                    </div>
                    <div class="hamburger-menu__nav">
                        <ul class="hamburger-nav">
                            <li class="hamburger-nav-list__item">
                                <a href="#">About</a>
                            </li>
                            <li class="hamburger-nav-list__item">
                                <a href="#">Community</a>
                            </li>
                            <li class="hamburger-nav-list__item">
                                <a href="#">Support</a>
                            </li>
                            <li class="hamburger-nav-list__item">
                                <a href="#" data-toggle="modal" data-target="#loginModal">Account</a>
                            </li>
                        </ul>
                    </div>
                    <div class="row">
                        <a href="#" class="logo__image pull-left">
                            <img src="img/logo.png" alt="Chesteria">
                        </a>

                        <ul class="circles-list m-l-100 pull-left">
                            <li>
                                <a href="#">
                                    <img src="img/icons/win.png" alt="Win">
                                </a>
                            </li>
                            <li>
                                <a href="#">
                                    <img src="img/icons/delay.png" alt="Delay">
                                </a>
                            </li>
                            <li>
                                <a href="#">
                                    <img src="img/icons/unlock.png" alt="Unlock">
                                </a>
                            </li>
                        </ul>
                        <nav class="menu-list hidden-xs hidden-sm hidden-md pull-right">
                            <ul>
                                <li class="menu-list__item">
                                    <a href="#">About</a>
                                </li>
                                <li class="menu-list__item">
                                    <a href="#">Community</a>
                                </li>
                                <li class="menu-list__item">
                                    <a href="#">Support</a>
                                </li>
                                <li class="menu-list__item">
                                    <a href="#" data-toggle="modal" data-target="#loginModal">Account</a>
                                </li>
                            </ul>
                        </nav>
                    </div>
                </div>
            </div>
        </section>
    </header>

    <section class="chests__section">
        <div class="container-fluid">
            <div class="row">
                <div class="container">
                    <div class="row">
                        <div class="col-xs-6 col-sm-3 col-md-3 col-lg-2">
                            <a href="#">
                                <div class="chestBox-won">
                                    <div class="chestBox-won__ribbon">
                                        You Won!
                                    </div>
                                    <div class="chestBox-won__btc">
                                        0.0148483
                                    </div>
                                    <div class="chestBox-won__delete-btn"></div>
                                </div>
                            </a>
                        </div>
                        <div class="col-xs-6 col-sm-3 col-md-3 col-lg-2">
                            <a href="#">
                                <div class="chestBox-locked">
                                    <div class="chestBox-locked__ribbon">
                                        Unlock?
                                    </div>
                                </div>
                            </a>
                        </div>
                        <div class="col-xs-6 col-sm-3 col-md-3 col-lg-2">
                            <a href="#">
                                <div class="chestBox-won">
                                    <div class="chestBox-won__ribbon">
                                        You Won!
                                    </div>
                                    <div class="chestBox-won__btc">
                                        0.0148483
                                    </div>
                                    <div class="chestBox-won__delete-btn"></div>
                                </div>
                            </a>
                        </div>
                        <div class="col-xs-6 col-sm-3 col-md-3 col-lg-2">
                            <a href="#">
                                <div class="chestBox-wonRuby">
                                    <div class="chestBox-wonRuby__ribbon">
                                        You Won!
                                    </div>
                                    <div class="chestBox-wonRuby__btc">
                                        0.0148483
                                    </div>
                                    <div class="chestBox-wonRuby__delete-btn"></div>
                                </div>
                            </a>
                        </div>
                        <div class="col-xs-6 col-sm-3 col-md-3 col-lg-2">
                            <a href="#">
                                <div class="chestBox-won">
                                    <div class="chestBox-won__ribbon">
                                        You Won!
                                    </div>
                                    <div class="chestBox-won__btc">
                                        0.0148483
                                    </div>
                                    <div class="chestBox-won__delete-btn"></div>
                                </div>
                            </a>
                        </div>
                        <div class="col-xs-6 col-sm-3 col-md-3 col-lg-2">
                            <a href="#">
                                <div class="chestBox-won">
                                    <div class="chestBox-won__ribbon">
                                        You Won!
                                    </div>
                                    <div class="chestBox-won__btc">
                                        0.0148483
                                    </div>
                                    <div class="chestBox-won__delete-btn"></div>
                                </div>
                            </a>
                        </div>
                        <div class="col-xs-6 col-sm-3 col-md-3 col-lg-2">
                            <a href="#">
                                <div class="chestBox-locked">
                                    <div class="chestBox-locked__ribbon">
                                        Unlock?
                                    </div>
                                </div>
                            </a>
                        </div>
                        <div class="col-xs-6 col-sm-3 col-md-3 col-lg-2">
                            <a href="#">
                                <div class="chestBox-won">
                                    <div class="chestBox-won__ribbon">
                                        You Won!
                                    </div>
                                    <div class="chestBox-won__btc">
                                        0.0148483
                                    </div>
                                    <div class="chestBox-won__delete-btn"></div>
                                </div>
                            </a>
                        </div>
                        <div class="col-xs-6 col-sm-3 col-md-3 col-lg-2">
                            <a href="#">
                                <div class="chestBox-locked">
                                    <div class="chestBox-locked__ribbon">
                                        Unlock?
                                    </div>
                                </div>
                            </a>
                        </div>
                        <div class="col-xs-6 col-sm-3 col-md-3 col-lg-2">
                            <a href="#">
                                <div class="chestBox-locked">
                                    <div class="chestBox-locked__ribbon">
                                        Unlock?
                                    </div>
                                </div>
                            </a>
                        </div>
                        <div class="col-xs-6 col-sm-3 col-md-3 col-lg-2">
                            <a href="#">
                                <div class="chestBox-locked">
                                    <div class="chestBox-locked__ribbon">
                                        Unlock?
                                    </div>
                                </div>
                            </a>
                        </div>
                        <div class="col-xs-6 col-sm-3 col-md-3 col-lg-2">
                            <a href="#">
                                <div class="chestBox-locked">
                                    <div class="chestBox-locked__ribbon">
                                        Unlock?
                                    </div>
                                </div>
                            </a>
                        </div>
                        <div class="col-xs-6 col-sm-3 col-md-3 col-lg-2">
                            <a href="#">
                                <div class="chestBox-won">
                                    <div class="chestBox-won__ribbon">
                                        You Won!
                                    </div>
                                    <div class="chestBox-won__btc">
                                        0.0148483
                                    </div>
                                    <div class="chestBox-won__delete-btn"></div>
                                </div>
                            </a>
                        </div>
                        <div class="col-xs-6 col-sm-3 col-md-3 col-lg-2">
                            <a href="#">
                                <div class="chestBox-locked">
                                    <div class="chestBox-locked__ribbon">
                                        Unlock?
                                    </div>
                                </div>
                            </a>
                        </div>
                        <div class="col-xs-6 col-sm-3 col-md-3 col-lg-2">
                            <a href="#">
                                <div class="chestBox-won">
                                    <div class="chestBox-won__ribbon">
                                        You Won!
                                    </div>
                                    <div class="chestBox-won__btc">
                                        0.0148483
                                    </div>
                                    <div class="chestBox-won__delete-btn"></div>
                                </div>
                            </a>
                        </div>
                        <div class="col-xs-6 col-sm-3 col-md-3 col-lg-2">
                            <a href="#">
                                <div class="chestBox-wonRuby">
                                    <div class="chestBox-wonRuby__ribbon">
                                        You Won!
                                    </div>
                                    <div class="chestBox-wonRuby__btc">
                                        0.0148483
                                    </div>
                                    <div class="chestBox-wonRuby__delete-btn"></div>
                                </div>
                            </a>
                        </div>
                        <div class="col-xs-6 col-sm-3 col-md-3 col-lg-2">
                            <a href="#">
                                <div class="chestBox-won">
                                    <div class="chestBox-won__ribbon">
                                        You Won!
                                    </div>
                                    <div class="chestBox-won__btc">
                                        0.0148483
                                    </div>
                                    <div class="chestBox-won__delete-btn"></div>
                                </div>
                            </a>
                        </div>
                        <div class="col-xs-6 col-sm-3 col-md-3 col-lg-2">
                            <a href="#">
                                <div class="chestBox-won">
                                    <div class="chestBox-won__ribbon">
                                        You Won!
                                    </div>
                                    <div class="chestBox-won__btc">
                                        0.0148483
                                    </div>
                                    <div class="chestBox-won__delete-btn"></div>
                                </div>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="statistic__section">
        <div class="container">
            <div class="row">
                <div class="statistic-content">
                    <div class="statistic-usersOnline">
                        <i class="fa fa-user" aria-hidden="true"></i>
                        Users online:
                        <span class="now">0</span>
                    </div>
                    <div class="statistic-chestWorth">
                        <i class="fa fa-btc" aria-hidden="true"></i>
                        Total placed chests worth:
                        <span class="worth">0</span> BTC
                    </div>
                    <div class="statistic-chestCommision">
                        <i class="fa fa-percent" aria-hidden="true"></i>
                        Chest's robbing commision:
                        <span class="percent">0.1%</span>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="about__section">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 col-md-12">
                    <div class="about-item bg-1">
                        <div class="row">
                            <div class="about-item__content">
                                <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3">
                                    <img class="about-item__thumb img-responsive" src="img/about/1.png" alt="Loading..">
                                </div>
                                <div class="col-md-9 col-lg-9">
                                    <h1>About
                                        <span>Chesteria</span>
                                    </h1>
                                    <h2>Chesteria – is an arcade style game, which is played against other players, using bitcoin
                                        currency. In this game you wouldn’t find any aspects of gambling, amount of bitcoin
                                        which you won there, depends from your selected game tactics, abilities to quickly
                                        respond to a situation, insight and just a little bit of luck.</h2>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="about-item bg-2">
                        <div class="row">
                            <div class="about-item__content">
                                <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3">
                                    <img class="about-item__thumb img-responsive" src="img/about/2.png" alt="Loading..">
                                </div>
                                <div class="col-md-9 col-lg-9">
                                    <h1>Select chest value and locate it on the game board..</h1>
                                    <h2>Seek other chests hidden by the players. If chest is secretly kept in a window and it
                                        is not locked, you will rob from it, how much is worth your chest. If there are less
                                        bitcoins in founded chest, than yours chest value, you will steal from it all amount!
                                        If chest is locked, you will just open it, but bitcoin you couldn’t thieve. If your
                                        chest will be robbed, it automatically will be deleted from the game board and if
                                        you wish to continue playing, you must to place another chest. Each new window you
                                        can open every 2 seconds.</h2>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="about-item bg-3">
                        <div class="row">
                            <div class="about-item__content">
                                <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3">
                                    <img class="about-item__thumb img-responsive" src="img/about/3.png" alt="Loading..">
                                </div>
                                <div class="col-md-9 col-lg-9">
                                    <h1>“Time Machine”</h1>
                                    <h2>– opportunity will reduce time between chests opening from 2 seconds to 1 second. You
                                        will be able to open two times more chests than usually. This ability lasts as long
                                        as your chest isn’t stolen. Price of this feature is equal to yours chest value.</h2>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="about-item bg-4">
                        <div class="row">
                            <div class="about-item__content">
                                <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3">
                                    <img class="about-item__thumb img-responsive" src="img/about/4.png" alt="Loading..">
                                </div>
                                <div class="col-md-9 col-lg-9">
                                    <h1>“Lock“</h1>
                                    <h2>– is a chance to lock your chest. When player founds your chest, he will just unlock
                                        it, but not steal. This ability can be used only one time per placed chest. Price
                                        of this feature is equal to yours chest value. Good Luck & Have Fun. You can contact
                                        us via support form.</h2>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="clearfix"></div>
            </div>
        </div>
    </section>

    <footer class="main__footer">
        <div class="container-fluid">
            <div class="main__footer-createdBy pull-left">
                All rights reserved.Chesteria 2017.
            </div>
            <div class="main__footer-sociallinks pull-right">
                <div class="socialinks-list">
                    <a href="#">
                        <img src="img/social/facebook.svg" alt="Facebook">
                    </a>
                    <a href="#">
                        <img src="img/social/instagram.svg" alt="Instagram">
                    </a>
                    <a href="#">
                        <img src="img/social/youtube.svg" alt="Youtube">
                    </a>
                    <a href="#">
                        <img src="img/social/twitter.svg" alt="Twitter">
                    </a>
                </div>
            </div>
        </div>
    </footer>

    <div class="hidden"></div>

    <script src="{{ mix('/js/all.js') }}"></script>

</body>

</html>