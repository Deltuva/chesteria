let mix = require('laravel-mix');
let bourbon = require('bourbon').includePaths;
let neat = require('bourbon-neat').includePaths;

mix.js([
        'resources/assets/js/app.js'
    ], 'public/js/all.js')
    .sass('resources/assets/sass/app.sass', 'public/css', null, {
        includePaths: bourbon.concat(neat)
    });

mix.disableNotifications();

if (mix.inProduction()) {
    mix.version();
}